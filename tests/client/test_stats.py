from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestStats(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.stats.get, return_value=Fabricator.success())
    def test(self, *args):
        res = self.orkg.stats.get()
        self.assertTrue(res.succeeded)
