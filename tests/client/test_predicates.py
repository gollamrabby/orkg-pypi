from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestPredicates(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(
        orkg.predicates.by_id, return_value=Fabricator.predicates.success_by_id("P1")
    )
    def test_by_id(self, *args):
        p_id = "P1"
        res = self.orkg.predicates.by_id(p_id)
        self.assertTrue(res.succeeded, f"Could not get the predicate by id {p_id}")

    @mock_test(orkg.predicates.get, return_value=Fabricator.predicates.success_get(10))
    def test_get(self, *args):
        size = 10
        res = self.orkg.predicates.get(size=size)
        self.assertTrue(res.succeeded)
        self.assertEqual(len(res.content), size)

    @mock_test(orkg.predicates.get_unpaginated, return_value=Fabricator.success_all())
    def test_get_unpaginated(self, *args):
        res = self.orkg.predicates.get_unpaginated(size=500)
        self.assertTrue(res.all_succeeded)

    @mock_test(
        orkg.predicates.add,
        return_value=Fabricator.predicates.success_add(label="Pr3D1c@t3 L4b3l"),
    )
    def test_add(self, *args):
        label = "Pr3D1c@t3 L4b3l"
        res = self.orkg.predicates.add(label=label)
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)

    @mock_test(
        orkg.predicates.add,
        return_value=Fabricator.predicates.success_add(label="Pr3D1c@t3 L4b3l"),
    )
    @mock_test(
        orkg.predicates.find_or_add,
        return_value=Fabricator.predicates.success_find_or_add(label="Pr3D1c@t3 L4b3l"),
    )
    def test_find_or_add(self, *args):
        label = "Pr3D1c@t3 L4b3l"
        old = self.orkg.predicates.add(label=label)
        self.assertTrue(old.succeeded, "Could not create the first predicate")
        self.assertEqual(
            old.content["label"],
            label,
            "The label of the first predicate doesn't match the correct label",
        )
        new = self.orkg.predicates.find_or_add(label=label)
        self.assertTrue(new.succeeded, "Could not find or add the second predicate")
        self.assertEqual(
            new.content["id"],
            old.content["id"],
            "The old and the new IDs are not the same",
        )

    @mock_test(
        orkg.predicates.add,
        return_value=Fabricator.predicates.success_add(id="Updatable", label="Before"),
    )
    @mock_test(
        orkg.predicates.update,
        return_value=Fabricator.predicates.success_update(
            id="Updatable", label="After"
        ),
    )
    @mock_test(
        orkg.predicates.by_id,
        return_value=Fabricator.predicates.success_by_id_from_cache("Updatable"),
    )
    def test_update(self, *args):
        res = self.orkg.predicates.add(label="Before")
        self.assertTrue(res.succeeded)
        label = "After"
        res = self.orkg.predicates.update(id=res.content["id"], label=label)
        self.assertTrue(res.succeeded)
        res = self.orkg.predicates.by_id(res.content["id"])
        self.assertTrue(res.succeeded)
        self.assertEqual(res.content["label"], label)
