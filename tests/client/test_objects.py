from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestObjects(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.objects.add, return_value=Fabricator.success())
    def test_add(self, *args):
        obj = {
            "predicates": [],
            "resource": {
                "name": "I rock maybe!",
                "classes": ["C2000"],
                "values": {
                    "P32": [{"@id": "R2"}],
                    "P55": [{"label": "ORKG is so cool!", "classes": ["C3000"]}],
                },
            },
        }
        res = self.orkg.objects.add(params=obj)
        self.assertTrue(res.succeeded)
