from unittest import TestCase

from orkg import ORKG, Hosts
from tests import Fabricator, mock_test


class TestHarvesters(TestCase):
    orkg = ORKG(Hosts.SANDBOX)

    @mock_test(orkg.harvesters.doi_harvest, return_value=Fabricator.success())
    def test_doi_harvest(self, *args):
        response = self.orkg.harvesters.doi_harvest(
            doi="https://api.test.datacite.org/dois/10.7484/s06c-8y98",
            orkg_rf="Computer Sciences",
        )
        self.assertTrue(response.succeeded)
