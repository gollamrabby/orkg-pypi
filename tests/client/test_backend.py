from unittest import TestCase, skip

from orkg import ORKG
from orkg.common import Hosts
from tests import Fabricator, mock_test


class TestBackend(TestCase):
    @mock_test(ORKG, return_value=Fabricator.orkg.dummy_class())
    @mock_test(ORKG.__init__)
    @mock_test(ORKG, "token", return_value=Fabricator.orkg.dummy_token())
    def test_auth(self, *args):
        orkg = ORKG(host="http://127.0.0.1:8080", creds=("test@test.test", "test123"))
        self.assertIsNotNone(orkg.token)

    @skip("TODO: Should figure out a way to mock this")
    def test_simcomp_host_assignment(self, *args):
        # check if simcomp host automatically assigned when it should be
        orkg = ORKG(host="https://sandbox.orkg.org/")
        self.assertIsNotNone(orkg.simcomp_host)

        # check if automatically assigned simcomp host actually works
        res = orkg.contributions.similar("R3005")
        self.assertTrue(res.succeeded)

        # check if simcomp host automatically assigned when it should not be
        orkg = ORKG(host="http://127.0.0.1:8080")
        self.assertIsNone(orkg.simcomp_host)

    @skip("TODO: Should figure out a way to mock this")
    def test_host_enum(self):
        # check if host name matches assigned host
        orkg = ORKG(host=Hosts.SANDBOX)
        self.assertEqual(orkg.host, "https://sandbox.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://sandbox.orkg.org/simcomp")

        # check if host actually works
        res = orkg.literals.get_all()
        self.assertTrue(res.succeeded)

        # check if host name matches assigned host
        orkg = ORKG(host=Hosts.PRODUCTION)
        self.assertEqual(orkg.host, "https://orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://orkg.org/simcomp")

        # check if host actually works
        res = orkg.literals.get_all()
        self.assertTrue(res.succeeded)

        # check if host name matches assigned host
        orkg = ORKG(host=Hosts.INCUBATING)
        self.assertEqual(orkg.host, "https://incubating.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://incubating.orkg.org/simcomp")

        # check if host actually works
        res = orkg.literals.get_all()
        self.assertTrue(res.succeeded)

    @skip("TODO: Should figure out a way to mock this")
    def test_mismatched_host_specification(self):
        # check if host/simcomp host name matches assigned host/simcomp host
        orkg = ORKG(
            host=Hosts.INCUBATING, simcomp_host="https://sandbox.orkg.org/simcomp"
        )
        self.assertEqual(orkg.host, "https://incubating.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://sandbox.orkg.org/simcomp")

        # check if paper in simcomp host
        # FIXME when mock tests are available, test paper that doesn't exist in both
        res = orkg.contributions.similar("R3005")
        self.assertTrue(res.succeeded)

    @skip("TODO: Should figure out a way to mock this")
    def test_host_errors(self):
        # using Hosts for simcomp host
        with self.assertRaises(AttributeError):
            ORKG(simcomp_host=Hosts.SANDBOX)

        # typo in host
        with self.assertRaises(AttributeError):
            ORKG(host=Hosts.SANBOX)

        # Hosts name as string
        with self.assertRaises(ValueError):
            ORKG(host="Hosts.SANDBOX")

        # missing http in host
        with self.assertRaises(ValueError):
            ORKG(host="sandbox.orkg.org/")

        # missing http in simcomp host
        with self.assertRaises(ValueError):
            ORKG(host=None, simcomp_host="sandbox.orkg.org/simcomp")

    @skip("TODO: Should figure out a way to mock this")
    def test_default_host(self):
        orkg = ORKG()
        self.assertEqual(orkg.host, "https://sandbox.orkg.org")
        self.assertEqual(orkg.simcomp_host, "https://sandbox.orkg.org/simcomp")
