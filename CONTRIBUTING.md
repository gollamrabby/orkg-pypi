Great to see you here! Here are a few things that you should know in order to make your experiance developing the ORKG python package as pleasent as possible.

## Getting Started

1. Checkout whatever issue you are happy with. For first timers we have the label `good first issue` to indicate what issues are ideal for new developers to the python package and can start off easy.
2. Create a branch for the issue. The branch should be named as the following: `<issue number>-<issue title - lower caps - only dashes>`
3. Commit messages should be clear as possible, and also avoid using past tense. e.g.: `Document feature X`

## Coding
Here are a set of coding notions we are using in the package:

1. use `poetry` to manage dependencies, You should install poetry like `pip install poetry`, then a simple `poetry install` will set up your environment.
2. if you have poetry you don't need to do `pip install -r requirements.txt`. Also if you have a new dependency, you can add it via `poetry add <package name>`.
3. use the `pre-commit` scripts to format the code. By applying `pre-commit install`.
4. once a new feature is added, a corresponding section in the documentation should be added directly under the `docs` directory.
5. use python 3.8 features as much as possible.
6. if you want to format strings use python 3.6+ f-strings.

## Merge Requests
Merge requests should be created for every issue, and should be linked to the issue. The merge request should be reviewed by at least one other developer before merging.
1. The merge request should be named clearly like: `Resolve "<issue title>"`, and when it is in a draft mode it should be named as: `Draft: Resolve "<issue title>"`.
2. Assign yourself as the (assignee) of the merge request.
3. Assign someone else as the (reviewer) of the merge request, and only do that when it is done and marked as ready.
4. If the merge request doesn't cover everything listed in the issue, it should clearly mention that in the description of the merge request, and possible open up a new issue with the missing features.
5. When creating the merge request check the `Delete source branch when merge request is accepted` checkbox, and uncheck `Squash commits when merge request is accepted`.

## Tests
All the functions of the python package should be tested.

Also, all tests should be mocked, and should not require any external dependencies (such as the Backend or SimComp).
For this the package provides a wrapper around the [UnitTest mock object library](https://docs.python.org/3/library/unittest.mock.html)

The package exposes a function `mock_test` which can be used to mock a function or an attribute. The function takes the following arguments:
- **target**: the target object/function to mock (Callable or str)
- **return_value**: the return value of the mocked function (Any)
- **side_effect**: the side effect of the mocked function (Callable or Exception)
- **attribute**: an optional attribute that should be mocked (str)

Here is a simple example of how it can be used
```python
@mock_test(orkg.resources.add, return_value=Fabricator.resources.success_add(label="test"))
def test_add_success(self, *args):
    label = "test"
    res = self.orkg.resources.add(label=label)
    self.assertTrue(res.succeeded)
    self.assertEqual(res.content["label"], label)
```

With this you are able to mock the output or the function implementation of any target.
`mock_test` is meant to be as least invasive as possible hence its usage as a decorator.
However, it can still be used as a context manager if needed.

```python
def test_add_fail(self):
    label = "test"
    with mock_test(self.orkg.resources.add, return_value=Fabricator.resources.fail_add(label=label)):
        res = self.orkg.resources.add(label=label)
        self.assertFalse(res.succeeded)
        self.assertEqual(res.status_code, "404")
```

Note the usage of `Fabricator` which contains all the methods necessary to make fake entities and inject them into the mock.

## Documentation
All features should be documented.

**Dev documentation:**
1. Include docstrings and typing hints in functions.
2. If other developers (but not users) need to be aware of a change or new feature, update this document.

**User documentation:**

The orkg-pypi repository contains a `docs` directory with reStructuredText (`.rst`) files describing how to use each client in the package, which are automatically published [here](https://orkg.readthedocs.io/en/latest/index.html).  When you make changes to the package, always update the documentation to reflect those changes!

1. If your change could break people's existing code, add it under Breaking Changes in `index.rst`.
2. If you've changed a parameter name or deprecated/removed a feature, update the appropriate table in `index.rst`.
3. If you've created a new client, create a corresponding `.rst` document in the `docs.source.client` directory.  Then add the client name to the list of connectors under Main Components and also to the table of contents under Individual Components, both in `client.rst`.
4. Add the new functionality to the corresponding client documentation. In general you should give a simple explanation of how a function works and include a code snippet showing how to use it, and potentially also the expected return value.  Also define the parameters if necessary.

See [here](https://docutils.sourceforge.io/docs/user/rst/quickref.html) for more details on using reStructuredText.
