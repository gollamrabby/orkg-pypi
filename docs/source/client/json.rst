ORKG JSON client
=========================

.. warning::

    Should be used by power-users only. This endpoint could have unaccounted consequences.

The SimComp service has an endpoint to store json objects to enable other services performing persistence features.

You need to have the `simcomp_host` defined to be able to use this client. If you instantiate the ORKG client using a web address or host environment name (``Hosts.PRODUCTION``, ``Hosts.SANDBOX``, or ``Hosts.INCUBATING``), the corresponding `simcomp_host` is automatically assigned.

However, if you are running the ORKG locally then you needC to specify `simcomp_host`.  See :ref:`Host <host-label>` for details.


We can access the json client directly to do the following:

Save JSON objects
^^^^^^^^^^^^^^^^^^^^^^^^^
With the following line of code you can easily create a dummy response.

.. code-block:: python

    ### all the parameters are required
    orkg.json.save_json(resource_id='R22225', json_object={'key': 'value'})
    >>> (Success)
    {
        "success":true
    }

Load JSON objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can perform a comparison of several contributions easily as well.

.. code-block:: python

    orkg.json.get_json(resource_id='R22225')
    >>> (Success)
    {
       "key": "value"
    }
