ORKG Resources
==============
Entities in the ORKG can be of type *Resource*, in order to be able to access this information or manipulate it (i.e., add, edit, delete) a *resources* component is added to the ORKG class to encapsulate the actions.

Having defined our entry point to the ORKG instance

.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the resources manager directly to do the following:

Getting resources by ID
^^^^^^^^^^^^^^^^^^^^^^^
You can get certain resources given that you know their ID value

.. code-block:: python

    ### Fetch resource by id
    orkg.resources.by_id(id='R0')
    >>> (Success)
    {
       "id":"R0",
       "label":"Gruber's design of ontologies",
       "created_at":"2019-01-06T15:04:07.692Z",
       "classes":[
       ],
       "shared":1,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

Getting resources by lookup
^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can lookup a resource by the label of the resource. A variety of parameter can be passed to specify an exact match, number of results to retrieve, and what order you need them in.

.. code-block:: python

    ### Fetch a list of resources
    # All parameters are optional
    # q: to search for a label containing
    # exact: to condition the search to as is
    # size: to specify the number of items in the page
    # sort: to specify the key to sort on
    # desc: to set the direction of sorting
    orkg.resources.get(q='ORKG',exact=False,size=30,sort='label',desc=True)
    >>> (Success)
    [
       {
          "id":"R125",
          "label":"ORKG system",
          "created_at":"2019-01-06T15:04:07.692Z",
          "classes":[
          ],
          "shared":1,
          "created_by":"00000000-0000-0000-0000-000000000000",
          "_class":"resource"
       },
       .....
    ]

Adding a new resource
^^^^^^^^^^^^^^^^^^^^^
The ORKG package can be used to create new resources in the ORKG instance you are connected to.

*Note: if you have you credentials entered in the ORKG instance creation all newly added resources will be credited to your user.*

.. code-block:: python

    ### Adds a resource to the graph
    # id: the id of the resource (optional)
    # label: the label of the resource
    # classes: the list of classes that a resource belong to (optional)
    orkg.resources.add(id='Custom_ID', label='new resource', classes=['Paper'])
    >>> (Success)
    {
       "id":"Custom_ID",
       "label":"new resource",
       "created_at":"2019-01-06T15:04:07.692Z",
       "classes":[
          "Paper"
       ],
       "shared":1,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

Updating an existing resource
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update an existing resource in the ORKG other than creating a new one. (***Use carefully***)

.. code-block:: python

    ### Updates a resource
    # id (mandatory): the id of the resource to change
    # label: the new label of the resource
    # classes: the list of new classes (optional)
    orkg.resources.update(id='R1', label='ORKG is so cool', classes=[])
    >>> (Success)
    {
       "id":"R1",
       "label":"ORKG is so cool",
       "created_at":"2019-01-06T15:04:07.692Z",
       "classes":[
       ],
       "shared":1,
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

Updating observatory and organization info of a resource
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can also update the observatory and organization of an existing resource in the ORKG. (***Use carefully***)

.. warning::

    Requires admin privileges. Will always display a warning that the endpoint needs elevated access rights.

.. code-block:: python

    ### Updates a resource
    # id (mandatory): the id of the resource to change
    # observatory_id (mandatory): the new observatory id
    # organization_id (mandatory): the new organization id
    orkg.resources.update_observatory(id='R1', observatory_id=new_obs, organization_id=new_org)
    >>> (Success)
    {
       "id":"R1",
       "label":"ORKG is so cool",
       "created_at":"2019-01-06T15:04:07.692Z",
       "classes":[
       ],
       "shared":1,
       "observatory_id": "SOMETHING_NEW_HERE",
       "organization_id": "ALSO_SOMETHING_NEW_HERE",
       "created_by":"00000000-0000-0000-0000-000000000000",
       "_class":"resource"
    }

Check if a resource exist
^^^^^^^^^^^^^^^^^^^^^^^^^
For your code to run smoothly you can check for the existence of resource before you update them for example. You can make sure that you code doesn't run into unexpected results.

.. code-block:: python

    ### Checks if resource exists
    # id: the id of the resource
    # returns a bool
    orkg.resources.exists(id='R1')
    >>> True


Deleting a resource
^^^^^^^^^^^^^^^^^^^^^^^^^
You can also delete a resource, if you have the rights to do so in the ORKG and if the resource is not used by any other statement.

.. warning::
    Some unintended results could happen. Delete operations should be handled with care.

.. code-block:: python

    orkg.resources.delete(id='R1')
    >> (Success) .....
    orkg.resources.exists(id='R1')
    >>> False


Getting a resource by id, label, or class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can get a resource by id, label, or label and class.  If a resource with the specified parameters does not exist, one will be created.  This function returns exactly one resource, even if multiple matching resources exist.

.. warning::
    If multiple classes are passed, only the first one will be searched.

.. code-block:: python

    ### find or create a new resource in the ORKG instance
    # id: the specific id to add (optional)
    # label: the label of the new resource (optional)
    # classes: list of classes to assign the resource to (optional)
    orkg.resources.find_or_add(label='new resource', classes=['Paper'])
    >> (Success)
    {
    'id': 'R267614',
    'label': 'new resource',
    'classes': ['Paper'],
    'shared': 0,
    'featured': False,
    'unlisted': False,
    'verified': False,
    'extraction_method': 'UNKNOWN',
    '_class': 'resource',
    'created_at': '2023-03-01T16:58:29.781Z',
    'created_by': '00000000-0000-0000-0000-000000000000',
    'observatory_id': '00000000-0000-0000-0000-000000000000',
    'organization_id': '00000000-0000-0000-0000-000000000000',
    'formatted_label': None
    }


Create a resource of tabular data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This function is provided in regard to support publishing statistical data in ORKG, and as a first step, we support two-dimensional tables like CSV files. And we are using the `RDF Data Cube Vocabulary <https://www.w3.org/TR/vocab-data-cube/>`_ to model the dataset inside the graph.

The function has three parameters :

1- file: csv file containing the table

2- label: the label of the output resource

3- dimensions: a list of column(s) name(s) used to identify the observations (ex. time, region), what the observation applies to.

*Note: The columns that are not in the dimensions list are considered as measures (the phenomenon being observed) and defining attribute (s) is not supported yet!.*

Behind the scenes there is a three-step process:

1- Parse the CSV file using the pandas library

2- Create Data Structure Definition

3- Create the observation resource for each row (the actual data in the cells of a table).

The final output is an ORKG Resource ID that can be used in defining contribution data of papers. And it has a tabular representation in the front-end.

Example:

Let's imagine this simple CSV file ‘example.csv’ where the first column is the dimension:

.. list-table:: example.csv
   :widths: 20 20 20 20 20
   :header-rows: 1

   * - Statistics
     - Event start time
     - Event end time
     - Duration
     - Sunrise
   * - Min
     - 09:05
     - 14:25
     - 04:41
     - 04:34
   * - Max
     - 12:17
     - 18:41
     - 06:32
     - 07:50

To create a resource for this table in ORKG we call the function:

.. code-block:: python

    ### Create a resource of a tabular data using RDF Datacube vocabulary starting from a CSV file
    # file: CSV file containing the table
    # label: label of the resource
    # dimensions: a list of column(s) name(s) that represent the dimensions.
    datasetID = save_dataset(file="example.csv", label="Summary", dimensions=["Statistics"])
    print(datasetID)
    >>> R11057
