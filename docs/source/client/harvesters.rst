ORKG Pyton Client Harvesters
============================

.. warning::

    This is an experimental feature. Bugs can happen and API may change completely in future releases.


The python client offers a variety of functions on top of ORKG content. One of these functions are the Harvesters.
Harvesters hides complex logic and enables users to seamlessly integrate ORKG content ingestion into their own systems and workflows.

At the moment, the client supports the following harvesters:

- [x] DOI Harvester
- [x] Directory Harverster (with DOI support)
- [ ] other harvesters are coming soon!

We start by defining our entry point for the harvesters.


.. code-block:: python

    from orkg import ORKG # import base class from package

    orkg = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG


We can access the harvesters manager directly to do the following:

DOI Harvesting
^^^^^^^^^^^^^^^^^^^^^^^
You need to know two things, the DOI where the content is located, and the ORKG research field to add this paper under

.. code-block:: python


   # Passing down the EXACT label of the ORKG's research field
   orkg.harvesters.doi_harvest(doi="https://api.test.datacite.org/dois/10.7484/s06c-8y98", orkg_rf="Computer Sciences")
   >>> {'id': 'R507726', 'label': 'Cover crops improve soil structure and change OC distribution in aggregate fractions', 'classes': ['Paper'], 'shared': 0, 'featured': False, 'unlisted': False, 'verified': False, 'extraction_method': 'UNKNOWN', '_class': 'resource', 'created_at': '2023-05-31T11:04:33.499726+02:00', 'created_by': '18a48c35-0a9d-4d35-b276-fe293f7d79c7', 'observatory_id': '00000000-0000-0000-0000-000000000000', 'organization_id': '00000000-0000-0000-0000-000000000000', 'formatted_label': None}

   # If you know the resource ID of the ORKG's research field you can pass it down as well
   from orkg import OID # import the ORKG ID class
   orkg.harvesters.doi_harvest(doi="https://api.test.datacite.org/dois/10.7484/s06c-8y98", orkg_rf=OID("R11"))
   >>> {'id': 'R507726', 'label': 'Cover crops improve soil structure and change OC distribution in aggregate fractions', 'classes': ['Paper'], 'shared': 0, 'featured': False, 'unlisted': False, 'verified': False, 'extraction_method': 'UNKNOWN', '_class': 'resource', 'created_at': '2023-05-31T11:04:33.499726+02:00', 'created_by': '18a48c35-0a9d-4d35-b276-fe293f7d79c7', 'observatory_id': '00000000-0000-0000-0000-000000000000', 'organization_id': '00000000-0000-0000-0000-000000000000', 'formatted_label': None}

**Note**: Searching for an exact label can cause problems if the label is not found. You can look up the research fields in the ORKG and use the ID instead.
