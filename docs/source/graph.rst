Graph
=====
Starting from version ``0.13.3`` we introduce a graph submodule.

One main functionality is now supported, that is, converting an ORKG component from RDF statements into
pythonic graph representation. We currently support the well known `networkx <https://networkx.org/>`_
as a representation.

Usage
^^^^^^

.. code-block:: python

    import orkg

    # create the client to the ORKG
    client = orkg.ORKG(host="<host-address-is-here>", creds=('email-address', 'password'))

    # get the subgraph of 'contribution_id'. The object has the type networkx.DiGraph
    subgraph = orkg.subgraph(client=client, thing_id='contribution_id')

    # try this and check what properties we provide!
    print('### Nodes ###')
    for node in subgraph.nodes(data=True):
        print(node)

    # also try this and validate the result!
    print('### Edges ###')
    for edge in subgraph.edges(data=True):
        print(edge)

API
^^^^
.. autofunction:: orkg.graph.subgraph
