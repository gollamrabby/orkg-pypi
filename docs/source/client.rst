Client
=======
Three main classes should be known when using the ORKG python package.

* ORKG class (main class to connect to an ORKG instance).
* OrkgResponse (output encapsulation for the ORKG API).
* OrkgUnpaginatedResponse (output encapsulation for pageable endpoints of the ORKG API)

Main Components
^^^^^^^^^^^^^^^
To instantiate an ORKG connector, you just add two lines:

.. code-block:: python

    from orkg import ORKG # import base class from package

    connector = ORKG(host="<host-address-is-here>", creds=('email-address', 'password')) # create the connector to the ORKG

the connector has many properties for facilitate access of specific features of the API

.. code-block:: python

    connector.resources # entry point to manipulate ORKG resources
    connector.predicates # entry point to manipulate ORKG predicates
    connector.classes # entry point to manipulate ORKG classes
    connector.literals # entry point to manipulate ORKG literals
    connector.stats # entry point to get ORKG statistics
    connector.statements # entry point to manipulate ORKG statements
    connector.papers # entry point to manipulate ORKG papers
    connector.comparisons # entry point to manipulate ORKG comparisons
    connector.objects # entry point to manipulate ORKG objects
    connector.templates # entry point to manipulate ORKG templates (Alpha)
    connector.harvesters # entry point to run harvesting functionalities on top of ORKG data (Alpha)

For the other main component which represent the output of all requests in the package.

.. code-block:: python

    connector.resources.get()  # this call will fetch a collection of resources from the ORKG
    >>> (Success) [{'id': 'R0', 'label': "Gruber's design of ontologies", 'created_at': '2019-01-06T15:04:07.692Z', 'classes': [], 'shared': 1, 'created_by': '00000000-0000-0000-0000-000000000000', '_class': 'resource'}, {'id': 'R172', 'label': 'Oceanography', 'created_at': '2019-01-06T15:04:07.692Z', 'classes': ['ResearchField'], 'shared': 1, 'created_by': '00000000-0000-0000-0000-000000000000', '_class': 'resource'},...]

Internally the OrkgResponse class contains information about the request and response from the API which can be accessed.

.. code-block:: python

    response = connector.resources.get() # get OrkgResponse object

    response.succeeded # returns bool if the call is a success or not
    >>> True

    response.status_code # get the HTTP status code of the request
    >>> 200

    response.url # get url of the request
    >>> https://orkg.org/api/resources/

    response.content # get response content as dict (JSON object) or list of dicts (JSON array)
    >>> [{'id': 'R0', 'label': "Gruber's design of ontologies", 'created_at': '2019-01-06T15:04:07.692Z', 'classes': [], 'shared': 1, 'created_by': '00000000-0000-0000-0000-000000000000', '_class': 'resource'}, {'id': 'R172', 'label': 'Oceanography', 'created_at': '2019-01-06T15:04:07.692Z', 'classes': ['ResearchField'], 'shared': 1, 'created_by': '00000000-0000-0000-0000-000000000000', '_class': 'resource'},...]

    response.page_info # retrieves the page information from the API, which contains the amount of items within the response and the total number of items in the graph.
    >>> {"pageable":{"sort":{"sorted":false,"unsorted":true,"empty":true},"pageNumber":0,"pageSize":20,"offset":0,"paged":true,"unpaged":false},"totalElements":20311,"totalPages":1016,"last":false,"first":true,"sort":{"sorted":false,"unsorted":true,"empty":true},"numberOfElements":20,"size":20,"number":0,"empty":false}


In case of calling a pageable endpoint, there is the possibility to retrieve all (or a range of) pages with one single call.
To achieve this, you just need to call the pageable method you need with the ``_unpaginated`` suffix in its name.
You can also define a range of pages you want to retrieve with the ``start_page`` and ``end_page`` arguments.

.. code-block:: python

    response = connector.resources.get_unpaginated(start_page=2, end_page=6) # get OrkgUnpaginatedResponse object

    response.all_succeeded # returns bool if the all calls succeeded
    >>> True

    len(response.responses) # get the number of calls/responses have been made
    >>> 5

    response.content # get the extended list of all responses' contents
    >>> [.....]

    for response in response.responses:
        response.url # get url of the request
        >>> https://orkg.org/api/resources/

        response.content # get response content as dict (JSON object) or list of dicts (JSON array)
        >>> [{'id': 'R0', 'label': "Gruber's design of ontologies", 'created_at': '2019-01-06T15:04:07.692Z', 'classes': [], 'shared': 1, 'created_by': '00000000-0000-0000-0000-000000000000', '_class': 'resource'}, {'id': 'R172', 'label': 'Oceanography', 'created_at': '2019-01-06T15:04:07.692Z', 'classes': ['ResearchField'], 'shared': 1, 'created_by': '00000000-0000-0000-0000-000000000000', '_class': 'resource'},...]

        response.page_info # retrieves the page information from the API, which contains the amount of items within the response and the total number of items in the graph.
        >>> {"pageable":{"sort":{"sorted":false,"unsorted":true,"empty":true},"pageNumber":0,"pageSize":20,"offset":0,"paged":true,"unpaged":false},"totalElements":20311,"totalPages":1016,"last":false,"first":true,"sort":{"sorted":false,"unsorted":true,"empty":true},"numberOfElements":20,"size":20,"number":0,"empty":false}


The response object of the python package also contains some helper methods to make the usage of the package easier. For example you can cast the output of the response to a pandas dataframe or a float value.
This is particularly useful when you want to use the output of the response directly in your code. So you won't need to access the dict object of the response and cast it manually.

.. code-block:: python

    response = connector.resources.by_id(id='R576003') # get OrkgResponse object

    response.succeeded # returns bool if the call is a success or not
    >>> True

    # Usually you would need to access the dict object of the response and cast it manually
    float(response.content['label'])
    >>> 50.21

    # With the helper methods you can do it directly
    connector.resources.by_id(id='R1').as_float()
    >>> 50.21

    # Similarly you can cast the response to a pandas dataframe
    df = connector.resources.by_id(id='R2').as_dataframe()
    type(df)
    >>> pandas.core.frame.DataFrame

You should note that some conversion helper method might take longer to execute due to the need of fetching additional data from the API.

At the moment here is a list of implemented helper methods:
- [x] as_float
- [x] as_int
- [x] as_dataframe
- [ ] as_string


Individual Components
^^^^^^^^^^^^^^^^^^^^^
For the individual components, they follow the same calls that are supported by the `ORKG API <http://tibhannover.gitlab.io/orkg/orkg-backend/api-doc/>`_.

For detailed list of all components


.. toctree::
    :maxdepth: 2

    client/resources
    client/literals
    client/predicates
    client/classes
    client/statements
    client/papers
    client/objects
    client/contributions
    client/json
    client/dummy
    client/templates
    client/harvesters
